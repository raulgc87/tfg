import styled from 'styled-components';

import Sello from '../assets/sello.png';

import Left from '../assets/lefthome.png';
import Right from '../assets/righthome.png';
import Bottom from '../assets/bottomhome.png';

// STYLES ------------------------------------------------------ //
const Container = styled.div`

`

const Hero = styled.div`
    max-width: 900px;
    margin: 0 auto;
    text-align: center;
    h1 {
        font-size: 3.5em;
        margin-bottom: 0;
        position: relative;
        &:before {
            content: '';
            height: 50px;
            width: 60px;
            position: absolute;
            top: -10px;
            right: -34px;
            background: url('/VectorTop.png') no-repeat center center;
        }
        &:after {
            content: '';
            height: 50px;
            width: 50px;
            position: absolute;
            bottom: -20px;
            left: 10px;
            background: url('/VectorBottom.png') no-repeat center center;
        }
    }
    p {}
`

const HeroImage = styled.div`
    display: flex;
    justify-content: center;
    margin: 32px 0px 132px;
    position: relative;
    .image-hero {
        &:after {
            content:'';
            background: url(${Bottom}) no-repeat center center;
            bottom: -20%;
            width: 460px;
            height: 270px;
            position: absolute;
            z-index: -2;
            right: 35%;
        }
        img {
            max-width: 100%;
        }
    }
    &:before {
        content:'';
        background: url(${Left}) no-repeat center center;
        left: 0;
        top: 0;
        width: 88px;
        height: 270px;
        position: absolute;
        z-index: -2;
    }
    &:after {
        content:'';
        background: url(${Right}) no-repeat center center;
        right: 0;
        top: 0;
        width: 340px;
        height: 270px;
        position: absolute;
        z-index: -2;
    }
`

const Button = styled.a`
    transition: all 0.2s ease-in;
    font-size: 0.9em;
    font-weight: bold;
    letter-spacing: 0.1em;
    border: 2px solid #000;
    background: #000;
    color: #fff;
    border-radius: 5em;
    padding: 14px 32px;
    text-transform: uppercase;
    text-decoration: none;
    display: inline-block;
    margin-top: 24px;
    &:hover{
        background: #fff;
        border: 2px solid #000;
        color: #000;
        cursor: pointer;
    } 
`

const CustomSection = styled.section`
    display: flex;
    justify-content: space-between;
    align-items: center;
    max-width: 1000px;
    margin: 0 auto;
    padding: 20px;
    &.reverse {
        flex-direction: row-reverse;
    }
    .description {
        padding: 0 32px;
    }
`

const Banner = styled.section`
    max-width: 1000px;
    margin: 100px auto;
    background: #F4F6D9;
    border: 3px solid #000;
    border-radius: 4px;
    display: flex;
    justify-content: space-between;
    position: relative;
    &:after {
        content: '';
        background: url(${Sello}) no-repeat center;
        background-size: cover;
        width: 290px;
        height: 112px;
        position: absolute;
        bottom: -55px;
        right: -140px;
    }
    .info {
        position: relative;
        padding: 50px;
        width: 100%;
        .title{
            margin: 0;
            max-width: 400px;
        }
        .description{
            margin-top: 8px;
            max-width: 400px;
        }
        &:before {
            content: '';
            background: #F28D66;
            width: 47px;
            height: 100%;
            position: absolute;
            top: 0;
            right: 0;
            border-left: 3px solid #000;
        }
        &:after {
            content: '';
            background: #6B887F;
            width: 20px;
            height: 100%;
            position: absolute;
            top: 0;
            right: 50px;
            border-left: 3px solid #000;
        }
    }
    .actions {
        background-image: radial-gradient(ellipse 50% 50% ,#000,#000 25%,#fff0 25%);
        background-size: 0.5em 0.5em;
        min-width: 32%;
        display: flex;
        justify-content: center;
        align-items: center;
        position: relative;
        border-left: 3px solid #000;
    }
`

const Opinion = styled.section``

export {
    Opinion,
    Banner,
    Container,
    Hero,
    HeroImage,
    Button,
    CustomSection
}