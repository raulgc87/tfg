import React from "react";
import { Link } from "wouter";
import {
  Opinion,
  Banner,
  Container,
  Hero,
  HeroImage,
  Button,
  CustomSection,
} from "./components";

// IMAGES
import FullBook from "../assets/book.png";
import ImageOne from "../assets/image_1.png";
import ImageSecond from "../assets/image_2.png";
import ImageThird from "../assets/image_3.png";

//SECTIONS
import Nav from "../common/navigator";
import Footer from "../common/footer";

const Home = () => {
  return (
    <>
      <Nav />
      <Container className="container">
        <Hero>
          <h1>Eternaliza tus momentos con tu cuaderno de viaje Pilgrim.</h1>
          <p>
            Crea tu propio y único cuarderno personalizado, porque un viaje no
            son solo fotos en la nube.
          </p>
          <a href="/cover">
            <Button>Crear libro</Button>
          </a>
        </Hero>
        <HeroImage>
          <div className="image-hero">
            <img src={FullBook} />
          </div>
        </HeroImage>
        <CustomSection>
          <div className="description">
            <h2>Añade tus fotos preferidas y personaliza cada página.</h2>
            <p>
              Pilgrin se encarga de ajustar las fotos a las diferentes
              creatividades que hemos preparado para ti.
            </p>
          </div>
          <img src={ImageOne} />
        </CustomSection>
        <CustomSection className="reverse">
          <div className="description">
            <h2>
              Todo cuaderno tiene sus notas, libera al escritor que tienes
              dentro.
            </h2>
            <p>
              Un cuaderno de viaje no son tan solo fotos, por eso Pilgrim tiene
              espacios dedicados para las notas que acompañaran a tus fotos de
              manera artística.
            </p>
          </div>
          <img src={ImageSecond} />
        </CustomSection>
        <CustomSection>
          <div className="description">
            <h2>Cómo tu, cada libro Pilgrim es único.</h2>
            <p>
              Pilgrim, te permite crear un libro artístico pero eres tú con tu
              personlaización quién lo hace único y diferente.
            </p>
          </div>
          <img src={ImageThird} />
        </CustomSection>
        <Banner>
          <div className="info">
            <h2 className="title">
              Eternaliza tus momentos con tu cuaderno de viaje Pilgrim.
            </h2>
            <div className="description">
              Crea tu propio y único cuarderno personalizado, porque un viaje no
              son solo fotos en la nube.
            </div>
          </div>
          <div className="actions">
            <Link href="/newBook">
              <Button>Crear libro</Button>
            </Link>
          </div>
        </Banner>
        <Opinion></Opinion>
        <Footer />
      </Container>
    </>
  );
};

export default Home;
