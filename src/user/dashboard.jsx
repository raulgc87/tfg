import React from 'react';
import { Link } from "wouter";
import styled from 'styled-components';

// IMAGES
import logo from '../assets/logo.svg';


const Dashboard = () => {
    
    const logOut = async () => {
        //Nos guardamos nuestro HOST
        const host = 'http://' + window.location.host;
        //Limpiamos Storage
        await localStorage.removeItem('userSession');
        //Reenviamos a la home
        window.location.href = host;
    };

    return (
        <>
            <Content>
                <aside>
                    <Link href="/"><img src={logo} alt="" style={{margin: 20}}/></Link>
                    <nav>
                        <ul>
                            <li>My profle</li>
                            <li>My orders</li>
                            <li className="active">My books</li>
                            <li>My drafts</li>
                            <li className="out"><span onClick={logOut}>Log out</span></li>
                        </ul>
                    </nav>
                </aside>
                <div className="container">
                    Hola
                </div>
            </Content>
        </>
    );
}

export default Dashboard;

// STYLES ------------------------------------------------------ //
const Content = styled.div`
    display: flex;
    flex-direction: row;
    min-height: 100vh;
    aside {
        min-width: 200px;
        ul {
            list-style: none;
            padding: 20px;
            li {
                border-radius: 4px;
                margin-bottom: 4px;
                padding: 8px;
                font-weight: bold;
                font-size: 14px;
                cursor: pointer;
                &.active {
                    background: #f5f5f5;
                    border-right: 4px solid #f28d66;
                }
                &:hover {
                    background: #f5f5f5;
                }
                &.out {
                    a {
                        color: #f28d66;
                        text-decoration: none;
                    }
                }
            }
        }
    }
    .container {
        width: 100%;
        display: flex;
        iframe {
            width: 100%;
            border: 0px;
            padding: 40px;
            background: #f5f5f5;
        }
    }
`