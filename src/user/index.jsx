import React, { useState, useEffect } from 'react';
import styled from 'styled-components';


//SECTIONS
import Dashboard from './dashboard';

// IMAGES
import logo from '../assets/logo.svg';

//LOGIN
import headersList from '../config/apiValidation';

const User = ({setUserId, setLoggedIn, loggedIn, userId}) => {
    
    //Estados que vamos a utilizar
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [userData, setUserData] = useState({});

    //Check si ya tenemos en sesión algun uID para mostrar o no el login
    useEffect(() => {

        const session = JSON.parse(localStorage.getItem('userSession'));

        if( session?.uId != null ) {
            console.log('estamos aquí', session)
            setLoggedIn(true);
        }

    }, [loggedIn]);

    
    //Acción para Login
    const loginSubmit = async () => {
        try {
          // Hacer llamada a la API para autenticar al usuario
          console.log(username, password);
          const response = await fetch("https://app.orbbyte.com/getWithFilters/04ae0c6647c93268946b1d1b5a6d876f225dd894e15d72761a72b0d66f14e902/login", {
            method: 'POST',
            headers: headersList,
            body: JSON.stringify({
                "email": username,
                "password": password
            })
          });
          
          const user = await response.json();
          
          if (!response.ok) {
              throw new Error('Inicio de sesión fallido');
            }
            
        // Revisamos que tenga contenido la respuesta de la API
          if (response.ok && user.data.length > 0) {
            console.log('Estamos aquí');
              // Decodificar la respuesta JSON
              setUserData(user.data[0]);
              setUserId(user.data[0].idPost);
            
              //Si la respuesta es satisfactoria marcamos que estamos logeados
              setLoggedIn(true);
            }
    
        } catch (error) {
          console.error('Error:', error);
        }
      };

    return (
        <>
            {loggedIn !== true ?
                <StyledLogin>
                    <form>
                        <img src={logo} />
                        <div>
                            <label htmlFor="username">Email</label>
                            <input type="email" id="username" value={username} onChange={e => setUsername(e.target.value)} />
                            <span>Formato de email incorrecto</span>
                        </div>
                        <div>
                            <label htmlFor="password">Contraseña:</label>
                            <input type="password" id="password" value={password} onChange={e => setPassword(e.target.value)} />
                        </div>
                        <button type="button" onClick={loginSubmit}>Iniciar sesión</button>
                    </form>
                </StyledLogin>
            : 
                <Dashboard />
            }
        </>
    );
}

export default User;

const StyledLogin = styled.div`
    background: url('https://images.unsplash.com/photo-1624627279963-1831ffa3d170?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1160&q=80');
    background-size: cover;
    min-height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
    form {
        box-shadow: 0 10px 20px rgba(0,0,0,0.5);
        display: flex;
        justify-content: center;
        background: #fff;
        flex-direction: column;
        padding: 20px;
        border-radius: 8px;
        width: 90%;
        max-width: 380px;
        label {
            display: block;
            font-weight: bold;
            margin-bottom: 8px;
        }
        img{
            height: 150px;
            margin-bottom: 32px;
        }
        div {
            input {
                min-height: 40px;
                width: 100%;
                margin-bottom: 24px;
                padding: 0 10px;
                box-sizing: border-box;
                border: 1px solid #919191;
                border-radius: 4px;
                &:invalid {
                    border-bottom: 2px solid red;
                    margin-bottom: 0;
                    & + span {
                        display: block;
                        color: red;
                        font-size: 12px;
                        margin: 8px 0 24px 0;
                    }
                }
            }
            span {
                display: none;
            }
        }
        button {
            transition: all 0.2s ease-in;
            font-size: 0.9em;
            font-weight: bold;
            letter-spacing: 0.1em;
            border: 2px solid #000;
            background: #000;
            color: #fff;
            border-radius: 5em;
            padding: 14px 32px;
            text-transform: uppercase;
            text-decoration: none;
            display: inline-block;
            margin-top: 24px;
            &:hover{
                background: #fff;
                border: 2px solid #000;
                color: #000;
                cursor: pointer;
            } 
        }
    }
`;