const headersList = {
    "Accept": "*/*",
    "User-Agent": "Thunder Client (https://www.thunderclient.com)",
    "Authorization": "Bearer eyJhbGciOiJIUzI1NiJ9.VTJGc2RHVmtYMTlRMHBOZFdxM3EwQ3loRnFvVC81NUh1U2RZMTZoazRGRHdaT1JIMEtQdWV4cUI3dE9IMjdiR2YveEdtcVFzRUJTMGZMNFdhMHBpdlVpRHJzdHRUdTEzRDlBekQxbGJzay9acGpGSEVrdllNYlNQc2wybkMrTytEcml2MC9XWGdaRnRWdW9PYnVVd3NJeHphVnhoZkY3by80UjBMNnJiNDRwV21VUDdQZGJaWHFURk5XL1hZSEprUWVtZG5LWnRzTUVGNld0bFk1NmRsTU5MSnV2WEJMSzhuemtiUmpYMGtzQlZvNGw4UE5XZTRMcEVlL2psbTBQYTgzZTA2d2Z4eDMwcWt0dmdKQXJjdlZnQlpyVmRHYmhEUVB2aWFYVzVhS2F1VE5ycmdXM0wxZEYyamQ5bTBpaDNVTjVGazh2R2lYTW1EYnRHL3hZUk8yOTJYbmNPUmRtU0NCeWJPQVhMcW8zK1hkKzQ1RDBldHpiVVljSUN0TlYw.bR2vM4CmSs0aZcTRr9-mVkGlohb3AtVnln6u5EnaeU4",
    "Content-Type": "application/json"
}

export default headersList;
   