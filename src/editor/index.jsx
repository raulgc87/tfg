import { useEffect } from "react";
import { v4 as uuid } from 'uuid';
import { Link } from "wouter"

const StartBook = () => {
    
    useEffect(() =>{
        //CREAMOS UNA uuID DEL LIBRO
        const storedBookID = localStorage.getItem('bookID');
        if (!storedBookID) {
          const newBookID = uuid();
          localStorage.setItem('bookID', newBookID);
        }
    }, []);

    return(
        <>
            <h1>Let's start a new Book</h1>
            <Link to='/newBook/cover'>Vamos!</Link>
        </>
    )
}

export default StartBook;