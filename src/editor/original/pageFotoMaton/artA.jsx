import React, {useEffect, useRef, useState} from 'react';

import bg from '../../../assets/book/page-17.webp';

const PageFotoMatonA = ({ firstImage, secondImage }) => {

    const canvasRef = useRef(null);


    function ArtWork({ctx}) {
        const img1 = new Image();
        img1.src = bg;

        return new Promise(resolve => {
            img1.onload = () => {
                ctx.drawImage(img1, 0, 0, 1200, 1200);
    
                resolve();
            };
        });
    }

    function ImageBg({ ctx, x, y, sx, sy }) {
        // Cargar la imagen
        const img = new Image();

        x = 0;
        y = 0;
        sx = 1200;
        sy = 1200;

        //Ponemos una imagen random hasta que no se cargue algo por parte del usuario
        img.src = firstImage;
        
        // Dibujar la imagen dentro del rectángulo
        return new Promise(resolve => {
          img.onload = () => {
            
            // Guardar el estado actual del contexto antes de aplicar la máscara
            ctx.save();
            
            // Definir la región del canvas que será visible usando ctx.clip()
            ctx.beginPath();
            ctx.rect(x, y, sx, sy);
            //Aplica máscara a la forma que dibujamos
            ctx.clip();
        
            // Dibujar la imagen en el canvas y ajustar su posición y tamaño
            const scaleFactor = Math.max(sx / img.width, sy / img.height);
            const imgWidth = img.width * scaleFactor;
            const imgHeight = img.height * scaleFactor;
            const imgX = x + (sx - imgWidth) / 2;
            const imgY = y + (sy - imgHeight) / 2;
            ctx.drawImage(img, imgX, imgY, imgWidth, imgHeight);
      
            // Restaurar el canvas para eliminar máscara al resto
            ctx.restore();
      
            // Resolver la promesa
            resolve();
          };
        });
    }

    function ImageOne({ ctx, x, y, sx, sy }) {
      // Cargar la imagen
      const img = new Image();
    
      x = 80;
      y = 90;
      sx = 240;
      sy = 240;
    
      //Ponemos una imagen random hasta que no se cargue algo por parte del usuario
      img.src = secondImage;
    
      // Dibujar la imagen dentro del rectángulo
      return new Promise(resolve => {
        img.onload = () => {
    
          // Guardar el estado actual del contexto antes de aplicar la máscara y rotar
          ctx.save();
    
          // Rotar todo el contexto
          const angle = -3; // en grados
          const radians = angle * Math.PI / 180;
          ctx.translate(x + sx / 2, y + sy / 2);
          ctx.rotate(radians);
          ctx.translate(-(x + sx / 2), -(y + sy / 2));
    
          // Definir la región del canvas que será visible usando ctx.clip()
          ctx.beginPath();
          ctx.rect(x, y, sx, sy);

          //Aplica máscara a la forma que dibujamos
          ctx.clip();
    
          // Dibujar la imagen en el canvas y ajustar su posición y tamaño
          const scaleFactor = Math.max(sx / img.width, sy / img.height);
          const imgWidth = img.width * scaleFactor;
          const imgHeight = img.height * scaleFactor;
          const imgX = x + (sx - imgWidth) / 2;
          const imgY = y + (sy - imgHeight) / 2;
    
          ctx.drawImage(img, imgX, imgY, imgWidth, imgHeight);
    
          // Restaurar el canvas para eliminar máscara y rotación
          ctx.restore();
    
          // Resolver la promesa
          resolve();
        };
      });
    }

    function ImageTwo({ ctx, x, y, sx, sy }) {
      // Cargar la imagen
      const img = new Image();
    
      x = 93;
      y = 340;
      sx = 240;
      sy = 240;
    
      //Ponemos una imagen random hasta que no se cargue algo por parte del usuario
      img.src = secondImage;
    
      // Dibujar la imagen dentro del rectángulo
      return new Promise(resolve => {
        img.onload = () => {
    
          // Guardar el estado actual del contexto antes de aplicar la máscara y rotar
          ctx.save();
    
          // Rotar todo el contexto
          const angle = -3; // en grados
          const radians = angle * Math.PI / 180;
          ctx.translate(x + sx / 2, y + sy / 2);
          ctx.rotate(radians);
          ctx.translate(-(x + sx / 2), -(y + sy / 2));
    
          // Definir la región del canvas que será visible usando ctx.clip()
          ctx.beginPath();
          ctx.rect(x, y, sx, sy);

          //Aplica máscara a la forma que dibujamos
          ctx.clip();
    
          // Dibujar la imagen en el canvas y ajustar su posición y tamaño
          const scaleFactor = Math.max(sx / img.width, sy / img.height);
          const imgWidth = img.width * scaleFactor;
          const imgHeight = img.height * scaleFactor;
          const imgX = x + (sx - imgWidth) / 2;
          const imgY = y + (sy - imgHeight) / 2;
    
          ctx.drawImage(img, imgX, imgY, imgWidth, imgHeight);
    
          // Restaurar el canvas para eliminar máscara y rotación
          ctx.restore();
    
          // Resolver la promesa
          resolve();
        };
      });
    }

    function ImageThree({ ctx, x, y, sx, sy }) {
      // Cargar la imagen
      const img = new Image();
    
      x = 100;
      y = 590;
      sx = 240;
      sy = 240;
    
      //Ponemos una imagen random hasta que no se cargue algo por parte del usuario
      img.src = secondImage;
    
      // Dibujar la imagen dentro del rectángulo
      return new Promise(resolve => {
        img.onload = () => {
    
          // Guardar el estado actual del contexto antes de aplicar la máscara y rotar
          ctx.save();
    
          // Rotar todo el contexto
          const angle = -3; // en grados
          const radians = angle * Math.PI / 180;
          ctx.translate(x + sx / 2, y + sy / 2);
          ctx.rotate(radians);
          ctx.translate(-(x + sx / 2), -(y + sy / 2));
    
          // Definir la región del canvas que será visible usando ctx.clip()
          ctx.beginPath();
          ctx.rect(x, y, sx, sy);

          //Aplica máscara a la forma que dibujamos
          ctx.clip();
    
          // Dibujar la imagen en el canvas y ajustar su posición y tamaño
          const scaleFactor = Math.max(sx / img.width, sy / img.height);
          const imgWidth = img.width * scaleFactor;
          const imgHeight = img.height * scaleFactor;
          const imgX = x + (sx - imgWidth) / 2;
          const imgY = y + (sy - imgHeight) / 2;
    
          ctx.drawImage(img, imgX, imgY, imgWidth, imgHeight);
    
          // Restaurar el canvas para eliminar máscara y rotación
          ctx.restore();
    
          // Resolver la promesa
          resolve();
        };
      });
    }

    function ImageFour({ ctx, x, y, sx, sy }) {
      // Cargar la imagen
      const img = new Image();
    
      x = 115;
      y = 840;
      sx = 240;
      sy = 240;
    
      //Ponemos una imagen random hasta que no se cargue algo por parte del usuario
      img.src = secondImage;
    
      // Dibujar la imagen dentro del rectángulo
      return new Promise(resolve => {
        img.onload = () => {
    
          // Guardar el estado actual del contexto antes de aplicar la máscara y rotar
          ctx.save();
    
          // Rotar todo el contexto
          const angle = -3; // en grados
          const radians = angle * Math.PI / 180;
          ctx.translate(x + sx / 2, y + sy / 2);
          ctx.rotate(radians);
          ctx.translate(-(x + sx / 2), -(y + sy / 2));
    
          // Definir la región del canvas que será visible usando ctx.clip()
          ctx.beginPath();
          ctx.rect(x, y, sx, sy);

          //Aplica máscara a la forma que dibujamos
          ctx.clip();
    
          // Dibujar la imagen en el canvas y ajustar su posición y tamaño
          const scaleFactor = Math.max(sx / img.width, sy / img.height);
          const imgWidth = img.width * scaleFactor;
          const imgHeight = img.height * scaleFactor;
          const imgX = x + (sx - imgWidth) / 2;
          const imgY = y + (sy - imgHeight) / 2;
    
          ctx.drawImage(img, imgX, imgY, imgWidth, imgHeight);
    
          // Restaurar el canvas para eliminar máscara y rotación
          ctx.restore();
    
          // Resolver la promesa
          resolve();
        };
      });
    }

    useEffect(() => {
        const canvas = canvasRef.current;
        const ctx = canvas.getContext("2d");

        // Increment canvasKey to recreate the canvas on each render

        async function draw() {
            await ImageBg({ctx});
            await ImageOne({ctx});
            await ImageTwo({ctx});
            await ImageThree({ctx});
            await ImageFour({ctx});
            await ArtWork({ctx});
        }
        
        draw();

    }, [ firstImage, secondImage ]);

    // Actualiza el estado canvasKey al montar el componente

    return <canvas ref={canvasRef} width={1200} height={1200} />;
};

export default PageFotoMatonA;