import React, {useEffect, useRef, useState} from 'react';
import {useForm} from "react-hook-form";
import {Link} from "wouter";

//PAGE COMPONENTS
import PageFotoMatonA from './artA';
import PageFotoMatonB from './artB';
import FormFotoMaton from './form';
import Layout from '../../common/layout';
import Stepper from '../../common/stepper';
import Actions from '../../common/actions';

import '../../book_editor.scss';
import './pageFotoMaton.scss';

export default function pageFotoMaton() {

    //Aquí ponemos las variables que iremos pasando entre componentes
    const [firstImage, setFirstImage] = useState();
    const [secondImage, setSecondImage] = useState();
    const [polaroid, setPolaroid] = useState();
    const [textPicture, setTextPicture] = useState();

    //This is an autosave while the elements changes, then you can get full data from localStorage
    useEffect(() => {

        const pOneData = {
            imageOne:firstImage,
            imageTwo:secondImage,
            imagePolaroid:polaroid,
            textPicture:textPicture
        };

        localStorage.setItem('pageFotoMaton', JSON.stringify(pOneData));

    }, [firstImage, secondImage, polaroid, textPicture]);

    return (
        <Layout
            form={
                <FormFotoMaton 
                    setFirstImage={setFirstImage} 
                    setSecondImage={setSecondImage}
                    setPolaroid={setPolaroid}
                    setTextPicture={setTextPicture}
                />
            }
            steps={
                <Stepper isActive="3" />
            }
            page={
                <>
                    <PageFotoMatonA 
                        firstImage={firstImage} 
                        secondImage={secondImage}
                    />
                    <PageFotoMatonB
                        polaroidImage={polaroid}
                        textPicture={textPicture}
                    />
                </>
            }
            actions={
                <Actions
                    firstImage={firstImage}
                    secondImage={secondImage}
                    polaroid={polaroid}
                    textPicture={textPicture}
                    goTo="pageThree"
                />
            }
        />
    )
}