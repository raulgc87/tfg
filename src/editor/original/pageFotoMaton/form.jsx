import React, { useState, useEffect } from 'react';
import {useForm} from "react-hook-form";
import ImageUpload from '../../common/fileUploader';

//Images
import imageOneDefault from '../../../assets/book/dummy/dog1.jpeg';
import imageTwoDefault from '../../../assets/book/dummy/dog14.jpeg';
import imageThreeDefault from '../../../assets/book/dummy/dog13.jpeg';

const FormFotoMaton = ({setFirstImage, setSecondImage, setPolaroid, setTextPicture}) => {
    const {register, watch} = useForm();

    //Miramos si existe el elemento en el localStorage
    const getPageBData = JSON.parse(localStorage.getItem('pageFotoMaton'));
    console.log('Hola', getPageBData);

    //Si existe, buscamos el title
    const defaultText = getPageBData != null ? getPageBData.textPicture : 'Lorem ipsum bla bla bla';
    //const pictDefatult = 'https://cdn.pixabay.com/photo/2020/03/23/08/45/cat-4959941_1280.jpg';
    
    //Miramos que existan las imagenes y si no es así ponemos una default
    useEffect(() => {
        setFirstImage(getPageBData != null ? getPageBData.imageOne : imageOneDefault );
        setSecondImage(getPageBData != null ? getPageBData.imageTwo : imageTwoDefault );
        setPolaroid(getPageBData != null ? getPageBData.imagePolaroid : imageThreeDefault );
    }, []);

    const titleLenght = String(watch('title')).length;

    return(
        <form id="pageOne">

            <ImageUpload refresh={setFirstImage} label="Imagen 1" />

            <ImageUpload refresh={setSecondImage} label="Imagen 2" />

            <ImageUpload refresh={setPolaroid} label="Polaroid" />

            
            <label className='inputName'>Polaroid text</label>
            <textarea 
                {...register('title')} 
                defaultValue={defaultText} 
                keypress={setTextPicture(watch('title'))}
                maxLength="120"
            />
            <span style={{display: 'block', float: 'right', fontSize: '10px'}}>{titleLenght}/35</span>
        </form>
    )
}

export default FormFotoMaton;