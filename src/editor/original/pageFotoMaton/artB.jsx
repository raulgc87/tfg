import React, {useEffect, useRef, useState} from 'react';

import bg from '../../../assets/book/page-18.webp';

const PageFotoMatonB = ({
        polaroidImage,
        textPicture
    }) => {

    const canvasRef = useRef(null);

    function ArtWork({ctx}) {
        const img1 = new Image();
        img1.src = bg;

        return new Promise(resolve => {
            img1.onload = () => {
                ctx.drawImage(img1, 0, 0, 1200, 1200);
    
                resolve();
            };
        });
    }
    
    function ImagePolaroid({ ctx, x, y, sx, sy }) {
        // Cargar la imagen
        const img = new Image();

        x = 350;
        y = 390;
        sx = 480;
        sy = 350;

        //Ponemos una imagen random hasta que no se cargue algo por parte del usuario
        img.src = polaroidImage;
        
        // Dibujar la imagen dentro del rectángulo
        return new Promise(resolve => {
          img.onload = () => {
            
            // Guardar el estado actual del contexto antes de aplicar la máscara
            ctx.save();
            
            // Definir la región del canvas que será visible usando ctx.clip()
            ctx.beginPath();
            ctx.rect(x, y, sx, sy);
            //Aplica máscara a la forma que dibujamos
            ctx.clip();
        
            // Dibujar la imagen en el canvas y ajustar su posición y tamaño
            const scaleFactor = Math.max(sx / img.width, sy / img.height);
            const imgWidth = img.width * scaleFactor;
            const imgHeight = img.height * scaleFactor;
            const imgX = x + (sx - imgWidth) / 2;
            const imgY = y + (sy - imgHeight) / 2;
            ctx.drawImage(img, imgX, imgY, imgWidth, imgHeight);
      
            // Restaurar el canvas para eliminar máscara al resto
            ctx.restore();
      
            // Resolver la promesa
            resolve();
          };
        });
      }

    async function TextImage({ctx}) {
        const maxWidth = 480;
        const lineHeight = 36;
        const x = 370;
        let y = 772;
        const text = textPicture != undefined ? textPicture : '';
        const words = text.split(' ');
        let line = '';
        
        //We have to be sure that the font is available
        await document.fonts.load('1em Attractype');
        
        for(let i = 0; i < words.length; i++) {
            const testLine = line + words[i] + ' ';
            const testWidth = ctx.measureText(testLine).width;
            
            if(testWidth > maxWidth) {
                ctx.fillText(line, x, y);
                line = words[i] + ' ';
                y += lineHeight;
            }
            else {
                line = testLine;
                ctx.font = `36px 'Attractype' `;
            }
        }
        ctx.fillText(line, x, y);
    }

    useEffect(() => {
        const canvas = canvasRef.current;
        const ctx = canvas.getContext("2d");

        // Increment canvasKey to recreate the canvas on each render

        async function draw() {
            await ImagePolaroid({ctx});
            await ArtWork({ctx});
            TextImage({ctx});
        }
        
        draw();

    }, [
        polaroidImage,
        textPicture
    ]);

    // Actualiza el estado canvasKey al montar el componente

    return <canvas ref={canvasRef} width={1200} height={1200} />;
};

export default PageFotoMatonB;