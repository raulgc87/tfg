import React, { useState, useEffect } from 'react';
import {useForm} from "react-hook-form";
import ImageUpload from '../../common/fileUploader';

//Images
import imageOneDefault from '../../../assets/book/dummy/dog11.jpeg';
import imageTwoDefault from '../../../assets/book/dummy/dog10.jpeg';
import imageThreeDefault from '../../../assets/book/dummy/dog7.jpeg';

const FormpageRoadTrip = ({setFirstImage, setSecondImage, setPolaroid, setTextPolaroid}) => {
    const {register, watch} = useForm();

    //Miramos si existe el elemento en el localStorage
    const getPageBData = JSON.parse(localStorage.getItem('pageRoadTrip'));
    
    //Si existe, buscamos el title
    const defaultText = getPageBData != null ? getPageBData.textPolaroid : 'Hola';
    //const pictDefatult = imageOneDefault;
    
    //Miramos que existan las imagenes y si no es así ponemos una default
    useEffect(() => {
        setFirstImage(getPageBData != null ? getPageBData.imageOne : imageOneDefault );
        setSecondImage(getPageBData != null ? getPageBData.imageTwo : imageTwoDefault );
        setPolaroid(getPageBData != null ? getPageBData.imagePolaroid : imageThreeDefault );
    }, []);

    const titleLenght = String(watch('title')).length;

    return(
        <form id="pageRoadTrip">

            <ImageUpload refresh={setFirstImage} label="Imagen 1" />

            <ImageUpload refresh={setSecondImage} label="Imagen 2" />

            <ImageUpload refresh={setPolaroid} label="Polaroid" />

            
            <label className='inputName'>Polaroid text</label>
            <textarea 
                {...register('title')} 
                defaultValue={defaultText} 
                keypress={setTextPolaroid(watch('title'))}
                maxLength="35"
            />
            <span style={{display: 'block', float: 'right', fontSize: '10px'}}>{titleLenght}/35</span>
        </form>
    )
}

export default FormpageRoadTrip;