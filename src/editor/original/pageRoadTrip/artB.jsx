import React, {useEffect, useRef, useState} from 'react';

import bg from '../../../assets/book/page-12.webp';

const PageRoadTripArtB = ({
        polaroidImage,
        textPolaroid,
        lengthTextPolaroid
    }) => {

    const canvasRef = useRef(null);

    function ArtWork({ctx}) {
        const img1 = new Image();
        img1.src = bg;

        return new Promise(resolve => {
            img1.onload = () => {
                ctx.drawImage(img1, 0, 0, 1200, 1200);
    
                resolve();
            };
        });
    }
    
    function ImagePolaroid({ ctx, x, y, sx, sy }) {
        // Cargar la imagen
        const img = new Image();

        x = 430;
        y = 380;
        sx = 520;
        sy = 520;

        //Ponemos una imagen random hasta que no se cargue algo por parte del usuario
        img.src = polaroidImage;
        
        // Dibujar la imagen dentro del rectángulo
        return new Promise(resolve => {
          img.onload = () => {
            
            // Guardar el estado actual del contexto antes de aplicar la máscara
            ctx.save();
            
            // Definir la región del canvas que será visible usando ctx.clip()
            ctx.beginPath();
            ctx.rect(x, y, sx, sy);
            //Aplica máscara a la forma que dibujamos
            ctx.clip();
        
            // Dibujar la imagen en el canvas y ajustar su posición y tamaño
            const scaleFactor = Math.max(sx / img.width, sy / img.height);
            const imgWidth = img.width * scaleFactor;
            const imgHeight = img.height * scaleFactor;
            const imgX = x + (sx - imgWidth) / 2;
            const imgY = y + (sy - imgHeight) / 2;
            ctx.drawImage(img, imgX, imgY, imgWidth, imgHeight);
      
            // Restaurar el canvas para eliminar máscara al resto
            ctx.restore();
      
            // Resolver la promesa
            resolve();
          };
        });
      }

    async function TextPolaroid({ctx}) {
        //Let's calculate the dinamic font-size
        // We provide the maxLenght, maxFontSize and check the wordLenght.
        ctx.fillStyle = 'black';
        let maxLength = 50;
        let maxFontSize = 100; 
        let length = Math.min(lengthTextPolaroid, maxLength);
        let fontSize = Math.round(maxFontSize - (maxFontSize / maxLength) * length);

        //Let set the smallest size allowed
        function rangeFontSize() { 
            if( fontSize <= 35 ) { 
                fontSize = '35'; 
                return(fontSize);
            } else { return(fontSize); }
        };

        //console.log(fontSize, rangeFontSize);

        //We have to be sure that the font is available
        await document.fonts.load('1em Attractype');

        //Then we can write whatever is needed
        ctx.font = `${rangeFontSize()}px 'Attractype' `;
        ctx.textAlign = "center";
        ctx.textBaseline = "middle";
        ctx.fillText(textPolaroid != undefined ? textPolaroid : '', 680, 965);

    }

    useEffect(() => {
        const canvas = canvasRef.current;
        const ctx = canvas.getContext("2d");

        // Increment canvasKey to recreate the canvas on each render

        async function draw() {
            await ImagePolaroid({ctx});
            await ArtWork({ctx});
            TextPolaroid({ctx});
        }
        
        draw();

    }, [
        polaroidImage,
        textPolaroid,
        lengthTextPolaroid 
    ]);

    // Actualiza el estado canvasKey al montar el componente

    return <canvas ref={canvasRef} width={1200} height={1200} />;
};

export default PageRoadTripArtB;