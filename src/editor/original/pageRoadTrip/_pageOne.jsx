import React, {useEffect} from 'react';
import { Link } from "wouter";
import {Helmet} from "react-helmet";
import './book_editor.scss';
import logo from '../assets/logo.svg';



export default function pageRoadTrip() {
    return (
        <>
            <Helmet>
                <script src="./js/pageRoadTrip.js"></script>
            </Helmet>
            <div id="editor">
                <aside>
                    <Link href="/">
                        <img src={logo} alt="" className="logo" />  
                    </Link>
                    
                    <div id="firstimage">
                        <label>First image:</label>
                    </div>
                    <div id="secondimage">
                        <label>Second image:</label>
                    </div>
                    <div id="polaroid">
                        <label>Polaroid:</label>
                    </div>
                    <div id="textPolaroid">
                        <label>Polaroid title:</label>
                    </div>
                </aside>
                <div className="bookeditor">
                <div className="stepper">
                        <ul>
                            <li>
                                <span>Portada</span>
                            </li>
                            <li class="active">
                                <span>1-2</span>
                            </li>
                            <li>
                                <span>3-4</span>
                            </li>
                            <li>
                                <span>5-6</span>
                            </li>
                            <li>
                                <span>7-8</span>
                            </li>
                            <li>
                                <span>9-10</span>
                            </li>
                            <li>
                                <span>11-12</span>
                            </li>
                            <li>
                                <span>13-14</span>
                            </li>
                            <li>
                                <span>15-16</span>
                            </li>
                        </ul>
                    </div>
                    <div id="editorbook">
                    </div>
                    <div className="actions">
                        <Link href="/">
                            <a className="text-button">Salir</a>
                        </Link>
                        <a href="/pageTwo" className="main-button" > 
                            Siguiente
                        </a>
                    </div>
                </div>
            </div>
        </>
    )
}
