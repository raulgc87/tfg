import {useEffect, useState} from 'react';

//PAGE COMPONENTS
import PageRoadTripArtA from './artA';
import PageRoadTripArtB from './artB';
import FormpageRoadTrip from './form';
import Layout from '../../common/layout';
import Stepper from '../../common/stepper';
import Actions from '../../common/actions';

import '../../book_editor.scss';
import './pageRoadTrip.scss';

export default function pageRoadTrip() {

    //Aquí ponemos las variables que iremos pasando entre componentes
    const [firstImage, setFirstImage] = useState();
    const [secondImage, setSecondImage] = useState();
    const [polaroid, setPolaroid] = useState();
    const [textPolaroid, setTextPolaroid] = useState();

    //This is an autosave while the elements changes, then you can get full data from localStorage
    useEffect(() => {

        const pOneData = {
            imageOne:firstImage,
            imageTwo:secondImage,
            imagePolaroid:polaroid,
            textPolaroid:textPolaroid
        };

        localStorage.setItem('pageRoadTrip', JSON.stringify(pOneData));

    }, [firstImage, secondImage, polaroid, textPolaroid]);

    return (
        <Layout
            form={
                <FormpageRoadTrip 
                    setFirstImage={setFirstImage} 
                    setSecondImage={setSecondImage}
                    setPolaroid={setPolaroid}
                    setTextPolaroid={setTextPolaroid}
                />
            }
            steps={
                <Stepper isActive="2" />
            }
            page={
                <>
                    <PageRoadTripArtA 
                        firstImage={firstImage} 
                        secondImage={secondImage}
                    />
                    <PageRoadTripArtB
                        polaroidImage={polaroid}
                        textPolaroid={textPolaroid}
                        lengthTextPolaroid={textPolaroid?.length || ''}
                    />
                </>
            }
            actions={
                <Actions
                    firstImage={firstImage}
                    secondImage={secondImage}
                    polaroid={polaroid}
                    textPolaroid={textPolaroid}
                    goTo="pageTwo"
                />
            }
        />
    )
}