import React, {useEffect, useRef, useState} from 'react';

//Images
import bg from '../../../assets/book/page-11.webp';

const PageRoadTripArtA = ({ firstImage, secondImage }) => {

    const canvasRef = useRef(null);


    function ArtWork({ctx}) {
        const img1 = new Image();
        img1.src = bg;

        return new Promise(resolve => {
            img1.onload = () => {
                ctx.drawImage(img1, 0, 0, 1200, 1200);
    
                resolve();
            };
        });
    }

    function ImageOne({ ctx, x, y, sx, sy }) {
        // Cargar la imagen
        const img = new Image();

        x = 85;
        y = 60;
        sx = 520;
        sy = 680;

        //Ponemos una imagen random hasta que no se cargue algo por parte del usuario
        img.src = firstImage;
        
        // Dibujar la imagen dentro del rectángulo
        return new Promise(resolve => {
          img.onload = () => {
            
            // Guardar el estado actual del contexto antes de aplicar la máscara
            ctx.save();
            
            // Definir la región del canvas que será visible usando ctx.clip()
            ctx.beginPath();
            ctx.rect(x, y, sx, sy);
            //Aplica máscara a la forma que dibujamos
            ctx.clip();
        
            // Dibujar la imagen en el canvas y ajustar su posición y tamaño
            const scaleFactor = Math.max(sx / img.width, sy / img.height);
            const imgWidth = img.width * scaleFactor;
            const imgHeight = img.height * scaleFactor;
            const imgX = x + (sx - imgWidth) / 2;
            const imgY = y + (sy - imgHeight) / 2;
            ctx.drawImage(img, imgX, imgY, imgWidth, imgHeight);
      
            // Restaurar el canvas para eliminar máscara al resto
            ctx.restore();
      
            // Resolver la promesa
            resolve();
          };
        });
    }

    function ImageTwo({ ctx, x, y, sx, sy }) {
        // Cargar la imagen
        const img = new Image();

        x = 525;
        y = 450;
        sx = 520;
        sy = 680;

        //Ponemos una imagen random hasta que no se cargue algo por parte del usuario
        img.src = secondImage;
        
        // Dibujar la imagen dentro del rectángulo
        return new Promise(resolve => {
          img.onload = () => {
            
            // Guardar el estado actual del contexto antes de aplicar la máscara
            ctx.save();
            
            // Definir la región del canvas que será visible usando ctx.clip()
            ctx.beginPath();
            ctx.rect(x, y, sx, sy);
            //Aplica máscara a la forma que dibujamos
            ctx.clip();
        
            // Dibujar la imagen en el canvas y ajustar su posición y tamaño
            const scaleFactor = Math.max(sx / img.width, sy / img.height);
            const imgWidth = img.width * scaleFactor;
            const imgHeight = img.height * scaleFactor;
            const imgX = x + (sx - imgWidth) / 2;
            const imgY = y + (sy - imgHeight) / 2;
            ctx.drawImage(img, imgX, imgY, imgWidth, imgHeight);
      
            // Restaurar el canvas para eliminar máscara al resto
            ctx.restore();
      
            // Resolver la promesa
            resolve();
          };
        });
    }

    useEffect(() => {
        const canvas = canvasRef.current;
        const ctx = canvas.getContext("2d");

        // Increment canvasKey to recreate the canvas on each render

        async function draw() {
            await ImageOne({ctx});
            await ImageTwo({ctx});
            await ArtWork({ctx});
        }
        
        draw();

    }, [ firstImage, secondImage ]);

    // Actualiza el estado canvasKey al montar el componente

    return <canvas ref={canvasRef} width={1200} height={1200} />;
};

export default PageRoadTripArtA;