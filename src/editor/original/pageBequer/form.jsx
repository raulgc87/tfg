import React, { useState, useEffect } from 'react';
import {useForm} from "react-hook-form";
import ImageUpload from '../../common/fileUploader';

//Images
import imageOneDefault from '../../../assets/book/dummy/dog16.jpeg';

const FormBequer = ({setFirstImage, setTextBg, setTextDoble, setTextImage}) => {
    const {register, watch} = useForm();

    //Miramos si existe el elemento en el localStorage
    const getPageBData = JSON.parse(localStorage.getItem('pBequerData'));
    console.log('Hola', getPageBData);

    //Si existe, buscamos el title
    const defaultTextImage = getPageBData != null ? getPageBData.textImage : 'Roma';
    const defaultTextBg = getPageBData != null ? getPageBData.textBg : 'Live';
    const defaultTextDoble = getPageBData != null ? getPageBData.textDoble : 'Every day like a dream';
    console.log('Recuperados texto a duplicar', defaultTextDoble);
    
    //Miramos que existan las imagenes y si no es así ponemos una default
    useEffect(() => {
        setFirstImage(getPageBData != null ? getPageBData.imageOne : imageOneDefault );
        //setSecondImage(getPageBData != null ? getPageBData.imageTwo : imageTwoDefault );
        //setPolaroid(getPageBData != null ? getPageBData.imagePolaroid : imageThreeDefault );
    }, []);

    return(
        <form id="pageOne">

            <label className='inputName'>Text Background</label>
            <input defaultValue={defaultTextBg} {...register('tBg')} keypress={setTextBg(watch('tBg'))} />
            <label className='inputName'>Text repited</label>
            <input defaultValue={defaultTextDoble} {...register('tDoble')} keypress={setTextDoble(watch('tDoble'))} />
            <label className='inputName'>Top picture text</label>
            <input defaultValue={defaultTextImage} {...register('tImage')} keypress={setTextImage(watch('tImage'))} />

            <ImageUpload refresh={setFirstImage} label="Foto rota" />
            
        </form>
    )
}

export default FormBequer;