import React, {useEffect, useRef, useState} from 'react';

import bg from '../../../assets/book/image-16.webp';
import imageOneDefault from '../../../assets/book/dummy/dog5.jpeg';

const PageBequerB = ({
        firstImage,
        textImage,
        lengthText
    }) => {

    console.log('la imagen:', firstImage);
    const canvasRef = useRef(null);

    function ArtWork({ctx}) {
        const img1 = new Image();
        img1.src = bg;

        return new Promise(resolve => {
            img1.onload = () => {
                ctx.drawImage(img1, 0, 0, 1200, 1200);
    
                resolve();
            };
        });
    }

    function ImageBg({ ctx, x, y, sx, sy }) {
        // Cargar la imagen
        const img = new Image();

        x = 220;
        y = 300;
        sx = 980;
        sy = 900;

        //Ponemos una imagen random hasta que no se cargue algo por parte del usuario
        img.src = firstImage;
        
        // Dibujar la imagen dentro del rectángulo
        return new Promise(resolve => {
          img.onload = () => {
            
            // Guardar el estado actual del contexto antes de aplicar la máscara
            ctx.save();
            
            // Definir la región del canvas que será visible usando ctx.clip()
            ctx.beginPath();
            ctx.rect(x, y, sx, sy);
            //Aplica máscara a la forma que dibujamos
            ctx.clip();
        
            // Dibujar la imagen en el canvas y ajustar su posición y tamaño
            const scaleFactor = Math.max(sx / img.width, sy / img.height);
            const imgWidth = img.width * scaleFactor;
            const imgHeight = img.height * scaleFactor;
            const imgX = x + (sx - imgWidth) / 2;
            const imgY = y + (sy - imgHeight) / 2;
            ctx.drawImage(img, imgX, imgY, imgWidth, imgHeight);
      
            // Restaurar el canvas para eliminar máscara al resto
            ctx.restore();
      
            // Resolver la promesa
            resolve();
          };
        });
      }

      async function TextImage({ctx}) {
        //Let's calculate the dinamic font-size
        // We provide the maxLenght, maxFontSize and check the wordLenght.
        let maxLength = 30;
        let maxFontSize = 400; 
        let length = Math.min(lengthText, maxLength);
        let fontSize = Math.round(maxFontSize - (600 / maxLength) * length);

        //We have to be sure that the font is available
        await document.fonts.load('1em Superion');

        //We get the color from object
        ctx.fillStyle = '#e6b601';

        //Let set the smallest size allowed
        function rangeFontSize() { 
            if( fontSize <= 20 ) { 
                fontSize = '20'; 
                return(fontSize);
            } else { return(fontSize); }
        };

        //Then we can write whatever is needed
        ctx.font = `${rangeFontSize()}px 'Superion' `;
        ctx.textAlign = "center";
        ctx.textBaseline = "middle";
        ctx.fillText(textImage != undefined ? textImage : '', 600, 250);

    }

    useEffect(() => {
        const canvas = canvasRef.current;
        const ctx = canvas.getContext("2d");

        // Increment canvasKey to recreate the canvas on each render

        async function draw() {
            await ImageBg({ctx});
            await ArtWork({ctx});
            TextImage({ctx});
        }
        
        draw();

    }, [
        firstImage,
        textImage,
        lengthText
    ]);

    // Actualiza el estado canvasKey al montar el componente

    return <canvas ref={canvasRef} width={1200} height={1200} />;
};

export default PageBequerB;