import React, {useEffect, useRef, useState} from 'react';

import bg from '../../../assets/book/image-15.webp';

const PageBequerA = ({
        textBg,
        textDoble,
        lengthBg
    }) => {

    const canvasRef = useRef(null);

    function ArtWork({ctx}) {
        const img1 = new Image();
        img1.src = bg;

        return new Promise(resolve => {
            img1.onload = () => {
                ctx.drawImage(img1, 0, 0, 1200, 1200);
    
                resolve();
            };
        });
    }

    async function TextDoble({ctx}) {
      const maxWidth = 1000;
      const lineHeight = 36;
      const x = 600;
      let y = 350;
      const text = textDoble != undefined ? textDoble : '';
      const words = text.split(' ');
      let line = '';
      
      //We have to be sure that the font is available
      await document.fonts.load('1em SWild');
      
      //We get the color from object
      ctx.fillStyle = '#fff';

      console.log('Texto a duplicar', textDoble);

        ctx.font = `120px 'SWild' `;
        const yPosition = 350;

        ctx.fillText(textDoble != undefined ? textDoble : '', x, yPosition);
        ctx.fillText(textDoble != undefined ? textDoble : '', x, yPosition + 90);
        ctx.fillText(textDoble != undefined ? textDoble : '', x, yPosition + 180);
        ctx.fillText(textDoble != undefined ? textDoble : '', x, yPosition + 270);
        ctx.fillText(textDoble != undefined ? textDoble : '', x, yPosition + 360);
        ctx.fillText(textDoble != undefined ? textDoble : '', x, yPosition + 450);
    }

    async function TextBackground({ctx}) {
        //Let's calculate the dinamic font-size
        // We provide the maxLenght, maxFontSize and check the wordLenght.
        let maxLength = 30;
        let maxFontSize = 900; 
        let length = Math.min(lengthBg, maxLength);
        let fontSize = Math.round(maxFontSize - (maxFontSize / maxLength) * length);

        //We have to be sure that the font is available
        await document.fonts.load('1em Brusio');

        //We get the color from object
        ctx.fillStyle = '#e6b601';

        //Let set the smallest size allowed
        function rangeFontSize() { 
            console.log(fontSize);
            if( fontSize <= 20 ) { 
                fontSize = '20'; 
                return(fontSize);
            } else { return(fontSize); }
        };

        //Then we can write whatever is needed
        ctx.font = `${rangeFontSize()}px 'Brusio' `;
        ctx.textAlign = "center";
        ctx.textBaseline = "middle";
        ctx.fillText(textBg != undefined ? textBg : '', 600, 650);
    };

    useEffect(() => {
        const canvas = canvasRef.current;
        const ctx = canvas.getContext("2d");

        // Increment canvasKey to recreate the canvas on each render

        async function draw() {
            await ArtWork({ctx});
            await TextBackground({ctx});
            await TextDoble({ctx});
        }
        
        draw();

    }, [
          textBg,
          textDoble,
          lengthBg
    ]);

    // Actualiza el estado canvasKey al montar el componente

    return <canvas ref={canvasRef} width={1200} height={1200} />;
};

export default PageBequerA;