import React, {useEffect, useRef, useState} from 'react';

//PAGE COMPONENTS
import PageBequerA from './artA';
import PageBequerB from './artB';
import FormBequer from './form';
import Layout from '../../common/layout';
import Stepper from '../../common/stepper';
import Actions from '../../common/actions';

import '../../book_editor.scss';
import './pageBequer.scss';

export default function PageTwo() {

    //Aquí ponemos las variables que iremos pasando entre componentes
    const [firstImage, setFirstImage] = useState();
    const [textImage, setTextImage] = useState();
    const [textBg, setTextBg] = useState();
    const [textDoble, setTextDoble] = useState();

    //This is an autosave while the elements changes, then you can get full data from localStorage
    useEffect(() => {

        const pBequerData = {
            imageOne:firstImage,
            textImage: textImage,
            textBg:textBg,
            textDoble:textDoble
        };

        localStorage.setItem('pBequerData', JSON.stringify(pBequerData));

    }, [firstImage, textBg, textDoble, textImage]);

    return (
        <Layout
            form={
                <FormBequer
                    setFirstImage={setFirstImage}
                    setTextBg={setTextBg}
                    setTextDoble={setTextDoble}
                    setTextImage={setTextImage}
                />
            }
            steps={
                <Stepper isActive="4" />
            }
            page={
                <>
                    <PageBequerA 
                        textBg={textBg}
                        textDoble={textDoble}
                        lengthBg={textBg?.length || ''}
                        lengthDoble={textDoble?.length || ''}
                    />
                    <PageBequerB
                        firstImage={firstImage}
                        textImage={textImage}
                        lengthText={textImage?.length || ''}
                    />
                </>
            }
            actions={
                <Actions
                    imageOne={firstImage}
                    textImage= {textImage}
                    textBg={textBg}
                    textDoble={textDoble}
                    goTo="pageThree"
                />
            }
        />
    )
}