import React, { useState, useEffect } from 'react';
import {useForm} from "react-hook-form";

const FormCover = ({setColorBook, setTitleBook}) => {
    const {register, watch} = useForm();

    //Miramos si existe el elemento en el localStorage
    const getCoverStorage = JSON.parse(localStorage.getItem('cover'));
    
    //Si existe, buscamos el title
    const DefaultTitle = getCoverStorage != null ? getCoverStorage.title : 'Roma';

    //Esto es una guarrada para que funcione el seteo del localStorage
    //Miramos si existe el color de Local, se selecciona y reenviamos el color al localStorage
    function getColorBook() {
        setTimeout(() => {
            setColorBook('');
        }, 100);
    }
    
    useEffect(() => {
        getColorBook();
    }, []);
    
    //Para el color seteamos como defaultChecked si coincide con el valor del input
    
    return(
        <form id="covercolor">
            <label className='inputName'>¿Dónde has viajado?</label>
            <input defaultValue={DefaultTitle} {...register('title')} keypress={setTitleBook(watch('title'))}
            />
            <label className='inputName'>Color de la portada</label>
            <div>
                <label htmlFor="color">
                    {getCoverStorage != null ?  
                        <input
                            {...register('color')}
                            name="color"
                            type="radio"
                            value="original"
                            onClick={ setColorBook(watch('color')) }
                            defaultChecked={getCoverStorage?.color === 'original'}
                        />
                    :
                        <input
                            {...register('color')}
                            name="color"
                            type="radio"
                            value="original"
                            onClick={ setColorBook(watch('color')) }
                            defaultChecked
                        />
                    }
                </label>
                <label htmlFor="color">
                    <input 
                        {...register('color')}
                        name="color"
                        type="radio"
                        value="blue"
                        onClick={ setColorBook(watch('color')) }
                        defaultChecked={getCoverStorage?.color === 'blue'}
                    />
                </label>
                <label htmlFor="color">
                    <input 
                        {...register('color')}
                        name="color"
                        type="radio"
                        value="pink"
                        onClick={ setColorBook(watch('color')) }
                        defaultChecked={getCoverStorage?.color === 'pink'}
                    />
                </label>
                <label htmlFor="color">
                    <input 
                        {...register('color')}
                        name="color"
                        type="radio"
                        value="green"
                        onClick={ setColorBook(watch('color')) }
                        defaultChecked={getCoverStorage?.color === 'green'}
                    />
                </label>
                <label htmlFor="red">
                    <input 
                        {...register('color')}
                        name="color"
                        type="radio"
                        value="red"
                        onClick={ setColorBook(watch('color')) }
                        defaultChecked={getCoverStorage?.color === 'red'}
                    />
                </label>
            </div>
        </form>
    )
}

export default FormCover;