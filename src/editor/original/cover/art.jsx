import React, {useEffect, useRef, useState} from 'react';

import bg from '../../../assets/bg-cover.webp';
import logo from '../../../assets/logo.png';

const CoverArt = ({titleBook, colorBook, lengthTitle}) => {

    const canvasRef = useRef(null);

    //Tenemos un objeto con toda la logica de colores, a partir de esto pintamos sin IF ni CASE
    const ColorParams = {
        blue: {
            background: '#0095de',
            text: '#6E4C41',
            blend: "overlay"
        },
        green:{
            background: '#6B887F',
            text: '#3D6271',
            blend: "overlay"
        },
        pink:{
            background: '#FF47B0',
            text: '#364968',
            blend: "overlay"
        },
        red:{
            background: '#F28D66',
            text: '#900048',
            blend: "lighten"
        },
        original:{
            background: '#CA9E67',
            text: '#3E333F',
            blend: "soft-light"
        }
    } 

    let ColorParamSelected = ColorParams[colorBook];

    function BgCover({ctx}) {
        const img1 = new Image();
        img1.src = bg;

        return new Promise(resolve => {
            img1.onload = () => {
                ctx.drawImage(img1, 0, 0, 1200, 1200);
    
                resolve();
            };
        });
    }

    function Logo({ctx}) {
        const img1 = new Image();
        img1.src = logo;

        return new Promise(resolve => {
            img1.onload = () => {
                ctx.drawImage(img1, 440, 240, 400, 400);
                resolve();
    
            };
        });
    }

    function BgColor({ctx}) { 

        //We get the color from object
        ctx.fillStyle = colorBook ? ColorParamSelected.background : '#CA9E67';

        //We get blend mode from object
        ctx.globalCompositeOperation = colorBook ? ColorParamSelected.blend : 'soft-light';
        
        ctx.rect(0, 0, 1200, 1200);
        ctx.fill();
        ctx.globalCompositeOperation = "source-over";
    }

    async function TitleBook({ctx}) {
        //Let's calculate the dinamic font-size
        // We provide the maxLenght, maxFontSize and check the wordLenght.
        let maxLength = 35;
        let maxFontSize = 300; 
        let length = Math.min(lengthTitle, maxLength);
        let fontSize = Math.round(maxFontSize - (maxFontSize / maxLength) * length);

        //We have to be sure that the font is available
        await document.fonts.load('1em Attractype');

        //We get the color from object
        ctx.fillStyle = colorBook ? ColorParamSelected.text : '#3E333F';

        //Then we can write whatever is needed
        ctx.font = `${fontSize}px 'Attractype' `;
        ctx.textAlign = "center";
        ctx.textBaseline = "middle";
        ctx.fillText(titleBook != undefined ? titleBook : '', 600, 900);

    }

    useEffect(() => {
        const canvas = canvasRef.current;
        const ctx = canvas.getContext("2d");

        // Increment canvasKey to recreate the canvas on each render

        async function draw() {
            await BgCover({ctx});
            BgColor({ctx});
            await Logo({ctx});
            TitleBook({ctx});
        }
        
        draw();

    }, [titleBook, colorBook, lengthTitle]);

    // Actualiza el estado canvasKey al montar el componente

    return <canvas ref={canvasRef} width={1200} height={1200} />;
};

export default CoverArt;