import {useEffect, useState} from 'react';

//PAGE COMPONENTS
import CoverArt from './art';
import FormCover from './form';
import Layout from '../../common/layout';
import Stepper from '../../common/stepper';
import Actions from '../../common/actions';

import '../../book_editor.scss';
import './cover.scss';

export default function Cover() {

    const [ColorBook, setColorBook] = useState();
    const [TitleBook, setTitleBook] = useState();

    //This is an autosave while the elements changes, then you can get full data from localStorage
    
    useEffect(() => {

        const coverData = {title:TitleBook, color:ColorBook};
        localStorage.setItem('cover', JSON.stringify(coverData));

    }, [ColorBook, TitleBook]);

    return (
        <Layout
            form={
                <FormCover 
                    setColorBook={setColorBook} 
                    setTitleBook={setTitleBook} 
                />
            }
            steps={ 
                <Stepper isActive="1" /> 
            }
            page={
                <CoverArt 
                    titleBook={TitleBook}
                    colorBook={ColorBook}
                    lengthTitle={TitleBook?.length || ''}
                />
            }
            actions={
                <Actions
                    title={TitleBook}
                    color={ColorBook}
                    goTo="pageOne"
                />
            }
        />
    )
}