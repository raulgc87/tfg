import React from "react";

export function Stepper({ isActive }) {
    return (
        <div className="stepper">
            <ul>
                <li className={ isActive === '1' ? 'active' : '' } >
                    <a href="/cover"><span>Portada</span></a>
                </li>
                <li className={ isActive === '2' ? 'active' : '' } >
                    <a href="/pageOne"><span>1-2</span></a>
                </li>
                <li className={ isActive === '3' ? 'active' : '' } >
                    <a href="/pageTwo"><span>3-4</span></a>
                </li>
                <li className={ isActive === '4' ? 'active' : '' } >
                    <a href="/pageThree"><span>5-6</span></a>
                </li>
                <li className={ isActive === '5' ? 'active' : '' } >
                    <a href="/pageFour"><span>7-8</span></a>
                </li>
                <li className={ isActive === '6' ? 'active' : '' } >
                    <a href="/pageFive"><span>9-10</span></a>
                </li>
                <li className={ isActive === '7' ? 'active' : '' } >
                    <a href="/pageSix"><span>11-12</span></a>
                </li>
                <li className={ isActive === '8' ? 'active' : '' } >
                    <a href="/pageSeven"><span>13-14</span></a>
                </li>
                <li className={ isActive === '9' ? 'active' : '' } >
                    <a href="/pageEight"><span>15-16</span></a>
                </li>
            </ul>
        </div>
    )
}

export default Stepper;