import React, {useState, useEffect} from 'react';

const ImageUpload = ({ refresh, label }) => {
    const [image, setImage] = useState(null);
  
    const handleFileInputChange = (event) => {
      const file = event.target.files[0];
      previewImage(file);
    };
  
    const previewImage = (file) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = () => {
        const imageUrl = reader.result;
        setImage(imageUrl);
      };
    };
  
    useEffect(() => {
      if (image) {
        refresh(image);
      }
    }, [image, refresh]);
    
    return (
        <>
            <label className='inputName'>{label}</label>
            <input type="file" onChange={handleFileInputChange} />
        </>
    );
};

export default ImageUpload;