import React from "react";

import Aside from './aside';


const Layout = ({ form, steps, page, actions }) => {
    return(
        <div id="editor">
            <Aside>
                {form}
            </Aside>
            <div className="bookeditor">
                {steps}
                <div id="editorbook">
                    {page}
                </div>
                <div className="actions">
                    {actions}
                </div>
            </div>
        </div>
    )
}

export default Layout;