import React, {useState} from "react";
import {Link, useLocation} from "wouter";

const Actions = (props) => {
    const [, navigate] = useLocation();
    const DefaultButtonValue = () => { return (<>Siguiente</>) }
    const LoadingButtonValue = () => { return (<>Cargando...</>) }
    const [textButton, setTextButton] = useState(< DefaultButtonValue />);

    const saveCover = async () => {
        try {
        await SaveDatalocalStorage().catch(error => {
            console.error('Error en primeraAccion:', error);
            setTextButton(DefaultButtonValue);
        });
        NextStep();
        } catch (error) {
          console.error('Error en handleClick:', error);
            // Manejar el error aquí
        }
      };
    
      const SaveDatalocalStorage = () => {
        return new Promise((resolve) => {
            //Indicamos mensaje de que estamos haciendo algo
            setTextButton(LoadingButtonValue);
          resolve();
        });
      };
    
      //Siguiente Pagina una vez que ha hecho todo...
      const NextStep = () => {
        console.log('redireccionamos')
        navigate(props.goTo); 
      };
    
    return(
        <div className="actions">
            <Link href="/" className="text-button">
                Salir
            </Link>
            <span  onClick={(e) => {
                saveCover(props);
            }} className="main-button">{textButton}</span>
        </div>
    )
}

export default Actions;