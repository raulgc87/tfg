import React from 'react';
import {Link} from "wouter";

import logo from '../../assets/logo.svg';

const Aside = ({children}) => {
    return (
        <aside>
            <Link href="/">
                <img src={logo}
                    alt=""
                    className="logo"/>
            </Link>
            { children }
        </aside>
    )
}

export default Aside;