import React from 'react';
import { Link } from "wouter";
import styled from 'styled-components';

// IMAGES
import logo from '../assets/logo.svg';
import Digital from '../assets/digital.jpg';
import TapaBlanda from '../assets/tapablanda.jpg';
import TapaDura from '../assets/tapadura.jpg';
import ImageThird from '../assets/image_3.png';

//SECTIONS
import Footer from '../common/footer';

const BookStyle = () => {
    return (
        <>
            <img src={logo} alt="" style={{margin: 20}}/>
            
            <Container>
                <h1>Selecciona el acabado</h1>
                <SelectProduct>
                    <li className="item">
                        <input type="radio" id="contactChoice1" name="contact" value="email" />
                        <img src={Digital} alt="" />
                        <label for="contactChoice1">Digital</label>
                        <ul>
                            <li>Descarga versión digital</li>
                            <li>Disponible online</li>
                        </ul>
                        <div className="price">5€</div>
                        <a className="line-button">Seleccionar</a>
                    </li>
                    <li className="item">
                        <input type="radio" id="contactChoice2" name="contact" value="email" />
                        <img src={TapaBlanda} alt="" />
                        <label for="contactChoice2">Tapa blanda</label>
                        <ul>
                            <li>Tapa blanda</li>
                            <li>Entrega en 15 días</li>
                            <li>Versión digital incluida</li>
                        </ul>
                        <div className="price">25€</div>
                        <a className="main-button">Seleccionar</a>
                    </li>
                    <li className="item">
                        <input type="radio" id="contactChoice3" name="contact" value="email" />
                        <img src={TapaDura} alt="" />
                        <label for="contactChoice3">Tapa dura</label>
                        <ul>
                            <li>Tapa dura</li>
                            <li>Entrega en 15 días</li>
                            <li>Versión digital incluida</li>
                        </ul>
                        <div className="price">30€</div>
                        <a className="line-button">Seleccionar</a>
                    </li>
                </SelectProduct>
            </Container>
            <FooterActions>
                    <Link href="/">
                        <a className="text-button">Salir</a>
                    </Link>
                    <a href="/checkout" className="main-button" > 
                        Finalizar
                    </a>
            </FooterActions>
        </>
    );
}

export default BookStyle;

// STYLES ------------------------------------------------------ //
const Container = styled.div`
    width: 100%;
    max-width: 1000px;
    margin: -100px auto 0;
    text-align: center;
    padding-bottom: 100px;
`
const FooterActions = styled.div`
    position: fixed;
    background: #fff;
    bottom: 0;
    left: 0;
    width: -moz-available;
    width: -webkit-fill-available;
    display: flex; 
    justify-content: space-between;
    padding: 20px;
    border-top: 1px solid #F5F5F5;
`

const SelectProduct = styled.ul`
    display: flex;
    justify-content: space-between;
    list-style: none;
    padding: 0;
    .item {
        max-width: 30%;
        width: 100%;
        padding: 32px;
        margin: 10px;
        border: 1px solid #ededed;
        border-radius: 8px;
        position: relative;
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        input{
            width: 100%;
            height: 100%;
            position: absolute;
            left: 0;
            top: 0;
            margin: 0;
            border-radius: 8px;
            -webkit-appearance: none;
            appearance: none;
            &:checked {
                border: 4px solid #6b887f;
                box-shadow: 0 4px 20px 3px rgba(0, 0, 0, 0.15)
            }
        }
        img{
           width: 100%;
        }
        label {
            font-size: 22px;
            font-weight: bold;
            margin: 12px 0;
            display: block;
        }
        ul{
            display: flex;
            flex-direction: column;
            align-items: center;
            padding: 0;
            list-style: none;
            li{
                position: relative;
                padding-left: 20px;
                margin-top: 12px;
                &:after {
                    content: '✔';
                    position: absolute;
                    left: 0;
                    top: 4px;
                    color: #8FC17E;
                    font-size: 12px;
                }
            }
        }
        .price {
            font-size: 42px;
            font-weight: bold;
            margin: 16px 0;
        }
    }
`