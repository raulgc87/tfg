import React from 'react';
import { Link } from "wouter";
import styled from 'styled-components';

// IMAGES
import logo from '../assets/logo.svg';
import TapaDura from '../assets/tapadura.jpg';
import Paypal from '../assets/PayPal.png';
import Stripe from '../assets/stripe.png';
import Card from '../assets/card.png';

//SECTIONS
import Footer from '../common/footer';

const Checkout = () => {
    return (
        <>
            <img src={logo} alt="" style={{margin: 20}}/>
            
            <Container>
                <h1>Finalizar compra</h1>
                <div className="banner">
                    <div className="thebook">
                        <img src={TapaDura} />
                        <div>
                            <strong>Pilgrim Book - Vilnius</strong>
                            <span>Tapa Blanda</span>
                        </div>
                    </div>
                    <div>
                        <span className="price">25€</span>
                    </div>
                </div>
                <form>
                    <div className="half">
                        <label htmlFor="">Nombre</label>
                        <input type="text" />
                    </div>
                    <div className="half">
                        <label htmlFor="">Apellidos</label>
                        <input type="text" />
                    </div>
                    <div className="half">
                        <label htmlFor="">Email</label>
                        <input type="text" />
                    </div>
                    <div className="half">
                        <label htmlFor="">Teléfono</label>
                        <input type="text" />
                    </div>
                    <div className="full">
                        <label htmlFor="">Dirección de envio</label>
                        <input type="text" />
                    </div>
                    <div className="half">
                        <label htmlFor="">Ciudad</label>
                        <input type="text" />
                    </div>
                    <div className="half">
                        <label htmlFor="">Código postal</label>
                        <input type="text" />
                    </div>
                    <div className="half">
                        <label htmlFor="">Provincia</label>
                        <input type="text" />
                    </div>
                    <div className="half">
                        <label htmlFor="">País</label>
                        <input type="text" />
                    </div>
                </form>

                <h2>Selecciona método de pago</h2>
                <ul className="payment">
                    <li>
                        <input type="radio" id="payment1" name="payment" value="payment" />
                        <img src={Paypal} alt=""/>
                        <span>Paypal</span>
                    </li>
                    <li>
                        <input type="radio" id="payment2" name="payment" value="payment" />
                        <img src={Stripe} alt=""/>
                        <span>Stripe</span>
                    </li>
                    <li>
                        <input type="radio" id="payment3" name="payment" value="payment" />
                        <img src={Card} alt=""/>
                        <span>Tarjeta</span>
                    </li>
                </ul>
                
            </Container>
            <FooterActions>
                    <Link href="/">
                        <a className="text-button">Salir</a>
                    </Link>
                    <a href="/user" className="main-button" > 
                        Finalizar
                    </a>
            </FooterActions>
        </>
    );
}

export default Checkout;

// STYLES ------------------------------------------------------ //
const Container = styled.div`
    width: 100%;
    max-width: 445px;
    margin: -100px auto 0;
    text-align: center;
    padding-bottom: 100px;
    .banner{
        display: flex;
        justify-content: space-between;
        border: 2px solid #6B887F;
        border-radius: 4px;
        background: #F5F5F5;
        width: -webkit-fill-available;
        width: -moz-available;
        padding: 10px;
        margin-bottom: 42px;
        .thebook {
            display: flex;
            div {
                text-align: left;
                font-size: 14px;
                strong {
                    display: block;
                }
            }
            img {
                width: 45px;
                margin-right: 24px;
            }
        }
        .price {
            font-weight: bold;
            font-size: 18px;
        }
    }
    form {
        display: flex;
        justify-content: space-between;
        flex-wrap: wrap;
        .half{
            width: 48%;
            text-align: left;    
        }
        .full {
            width: 100%;
            text-align: left;    
        }
        input {
            width: -webkit-fill-available;
            width: -moz-available;
            margin-top: 8px;
            margin-bottom: 24px;
            border: 1px solid #DEDEDE;
            border-radius: 4px;
            padding: 8px;
       }
       label {
           font-weight: bold;
           font-size: 12px;
       }
    }
    h2 {}
    .payment{
        list-style: none;
        padding: 0;
        display: flex;
        justify-content: space-between;
        li{
            max-width: 20%;
            width: 100%;
            border: 1px solid #DEDEDE;
            border-radius: 4px;
            padding: 24px;
            display: flex;
            flex-direction: column;
            position: relative;
            align-items: center;
            justify-content: space-between;
            span {
                font-weight: bold;
                font-size: 14px;
            }
            input {
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                -webkit-appearance: none;
                appearance: none;
                &:checked {
                    border: 4px solid #6b887f;
                    box-shadow: 0 4px 20px 3px rgba(0, 0, 0, 0.15);
                    margin: 0;
                }
            }
            img{
                max-height: 47px;
                max-width: 85px;
            }
            &:nth-child(3){
                img{
                    max-width: 42px;
                }
            }
        }

    }
`
const FooterActions = styled.div`
    position: fixed;
    bottom: 0;
    left: 0;
    width: -moz-available;
    width: -webkit-fill-available;
    display: flex; 
    justify-content: space-between;
    padding: 20px;
    border-top: 1px solid #F5F5F5;
    background: #fff;
`