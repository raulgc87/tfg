import React from "react";
import styled from 'styled-components';
import { Link } from "wouter";

import logo from '../assets/logo.svg';

const Nav = () => {
  return (
    <Header>
      <Link to="/">
        <img className="logo" src={logo} />
      </Link>
      <nav>
        <Link to="/user" className="text-button">
          Login
        </Link>
        <Link to="/newBook" className="main-button">
          Crear Libro
        </Link>
        <Link to="#" className="line-button">
          <CartIcon />
        </Link>
      </nav>
    </Header>
  );
}

export default Nav;

//Styles
const Header = styled.header`
  width: auto;  
  padding: 20px;
  display: flex;
  justify-content: space-between;
  position: sticky;
  top: 0;
  .logo {
    cursor: pointer;
  }
  nav{
    display: flex;
    align-items: flex-start;
    a{
      margin-left: 8px;
      &.main-button {
        margin-top: 24px;
      }
      &.line-button {
          margin-top: 24px;
          padding: 4.5px 7px;
      }
      &.text-button {
          margin-top: 24px;
      }
    }
  }
`

const CartIcon = () => 
<svg width="20" height="22" viewBox="0 0 20 22" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M1.88405 7.9199C1.9258 7.40035 2.35961 7 2.88084 7H16.7851C17.3064 7 17.7402 7.40035 17.7819 7.9199L18.6594 18.8398C18.7529 20.0038 17.8336 21 16.6658 21H3.00013C1.8324 21 0.913016 20.0038 1.00656 18.8398L1.88405 7.9199Z" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M13.833 10V5C13.833 2.79086 12.0421 1 9.83301 1C7.62387 1 5.83301 2.79086 5.83301 5V10" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
</svg>;