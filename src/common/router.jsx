import React, { useState, useEffect } from 'react';
import {Router, Switch, Route } from "wouter";

import Home from '../home/index';
import BookStyle from '../common/bookstyle';
import Checkout from '../common/checkout';
import User from '../user/index';
import StartBook from '../editor';
import Cover from '../editor/original/cover/index';
import PageOne from '../editor/original/pageRoadTrip/index';
import PageTwo from '../editor/original/pageFotoMaton/index';
import PageThree from '../editor/original/pageBequer/index';
  

const ContentApp = () => {

  const [userId, setUserId] = useState(JSON.parse(localStorage.getItem('userSession'))?.uId || null);
  const [loggedIn, setLoggedIn] = useState(false);

  useEffect(() => {
    //Miramos si el usuario ha hecho login para mantener sesión
    if (userId) {
      const userSession = {
        uId: userId
      };
      localStorage.setItem('userSession', JSON.stringify(userSession));
    }
  }, [ userId ]);

  return (
    <div className="content">
        <Switch>
          <Route path="/" component={Home} />
          <Route path="/users/:name">
              {(params) => <div>Hello, {params.name}!</div>}
          </Route>
          <Route path="/newBook" component={StartBook} />
          <Route path="/newBook/cover" component={Cover} />
          <Route path="/newBook/pageOne" component={PageOne} />
          <Route path="/newBook/pageTwo" component={PageTwo} />
          <Route path="/newBook/pageThree" component={PageThree} />
          <Route path="/newBook/bookstyle" component={BookStyle} />
          <Route path="/checkout" component={Checkout} />
          <Route path="/user">
            <User setUserId={setUserId} setLoggedIn={setLoggedIn} loggedIn={loggedIn} userId={userId}/>
          </Route>
        </Switch>
    </div>
  );
}

export default ContentApp;